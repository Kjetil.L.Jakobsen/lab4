package cellular;

import java.lang.Thread.State;
import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

    @Override
	public int numberOfRows() {
		return this.currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return this.currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return this.currentGeneration.get(row, col);
	}

    @Override
	public void step() {
		//Copy the grid and update every cell
		IGrid nextGeneration = currentGeneration.copy();
		for (int row = 0; row < numberOfRows(); row++) {
			for (int col = 0; col < numberOfColumns(); col++) {
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}
		this.currentGeneration = nextGeneration;
	}

    private int countNeighbors(int row, int col, CellState state) {
		// Avoid relying on exceptions, need to know height and width of grid
		int heigth = numberOfRows();
		int width = numberOfColumns();
		int count = 0;
		if (getCellState(row, col) == state) {
			count--;
		}
		//Count neighbors
		for (int i = -1; i < 2; i++) {
			if (row+i < 0 || row+i >= heigth) {
				continue;
			}
			for (int j = -1; j < 2; j++) {
				if (col+j < 0 || col+j >= width) {
					continue;
				}
				if (getCellState(row+i, col+j) == state) {
					if (row+i < 0) {
					}
					count++;
				}
			}
		}
		return count;
	}

    @Override
    public CellState getNextCell(int row, int col) {
        if (getCellState(row, col) == CellState.ALIVE) {
            return CellState.DYING;
        }
        else if (getCellState(row, col) == CellState.DYING) {
            return CellState.DEAD;
        }
        else if (countNeighbors(row, col, CellState.ALIVE) == 2) {
            return CellState.ALIVE;
        }
        return CellState.DEAD;
    }

    @Override
	public IGrid getGrid() {
		return currentGeneration;
	}
	
}
